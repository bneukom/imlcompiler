package ch.fhnw.imlcompiler.scanner;

public class LexicalParserException extends RuntimeException {

	public LexicalParserException(String text) {
		super(text);
	}

}
