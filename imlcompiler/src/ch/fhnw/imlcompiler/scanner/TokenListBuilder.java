package ch.fhnw.imlcompiler.scanner;

import java.util.ArrayList;
import java.util.List;

import ch.fhnw.imlcompiler.scanner.ImlScanner.IdentifierToken;
import ch.fhnw.imlcompiler.scanner.ImlScanner.IntegerLiteral;
import ch.fhnw.imlcompiler.scanner.ImlScanner.Terminal;
import ch.fhnw.imlcompiler.scanner.ImlScanner.Token;

public class TokenListBuilder {
	private final List<Token> tokens = new ArrayList<>();

	public TokenListBuilder literal(int value) {
		tokens.add(new IntegerLiteral(value));
		return this;
	}

	public TokenListBuilder terminal(Terminal terminal) {
		tokens.add(new Token(terminal));
		return this;
	}
	
	public TokenListBuilder ident(String ident) {
		tokens.add(new IdentifierToken(ident));
		return this;
	}

	public List<Token> build() {
		return tokens;
	}

}
