package ch.fhnw.imlcompiler.scanner;

import java.util.LinkedList;
import java.util.List;

public class ImlScanner {

	private StringBuilder lexicalAccumulator = new StringBuilder();
	private long numericalAccumulator = 0;

	private static final int DEFAULT_STATE = 0;
	private static final int ALPHANUMERIC_STATE = 1;
	private static final int NUMERICAL_STATE = 2;
	private static final int SYMBOL_STATE = 3;

	public List<Token> scan(CharSequence input) {
		assert input.length() == 0 || input.charAt(input.length() - 1) == '\n';

		final List<Token> tokenList = new LinkedList<>();

		int scannerState = DEFAULT_STATE;

		for (int inputIndex = 0; inputIndex < input.length(); ++inputIndex) {
			char currentChar = input.charAt(inputIndex);

			switch (scannerState) {
			case DEFAULT_STATE:
				if (currentChar >= 65 && currentChar <= 122) {
					// [a-Z]
					scannerState = ALPHANUMERIC_STATE;
					accumulateLexical(currentChar);
				} else if (Character.isDigit(currentChar)) {
					// [0-9]
					scannerState = NUMERICAL_STATE;
					accumulateIntLiteral(currentChar);
				} else if (Symbol.startsWith(currentChar)) {
					// symbol (<=, >, &&, etc...)
					scannerState = SYMBOL_STATE;
					accumulateLexical(currentChar);
				} else if (Character.isWhitespace(currentChar)) {
					// ignored
				} else {
					throw new LexicalParserException("invalid character");
				}
				break;
			case ALPHANUMERIC_STATE:
				if (isNumerical(currentChar) || isAlphaNumerical(currentChar)) {
					scannerState = ALPHANUMERIC_STATE;

					accumulateLexical(currentChar);

				} else {
					scannerState = DEFAULT_STATE;

					inputIndex = inputIndex - 1;
					final String terminal = lexicalAccumulator.toString();

					// is a keyword
					if (Keyword.contains(terminal)) {

						final String keyword = terminal;
						final Token keywordToken = Keyword.get(keyword).create();
						tokenList.add(keywordToken);

					} else {
						// new identifier
						tokenList.add(new IdentifierToken(lexicalAccumulator.toString()));
					}

					lexicalAccumulator = new StringBuilder();
				}
				break;
			case NUMERICAL_STATE:
				if (Character.isDigit(currentChar)) {
					scannerState = NUMERICAL_STATE;

					accumulateIntLiteral(currentChar);

				} else {
					tokenList.add(new IntegerLiteral(((int) numericalAccumulator)));

					scannerState = 0;

					// one back for next lexeme
					inputIndex = inputIndex - 1;

					numericalAccumulator = 0;
				}
				break;
			case SYMBOL_STATE:
				accumulateLexical(currentChar);
				String symbol = lexicalAccumulator.toString();

				if (!Symbol.contains(symbol)) {
					symbol = String.valueOf(symbol.charAt(0));

					// one back for next lexeme
					inputIndex = inputIndex - 1;
				}

				final Token symbolToken = Symbol.get(symbol).create();
				tokenList.add(symbolToken);

				scannerState = DEFAULT_STATE;

				lexicalAccumulator = new StringBuilder();
				
				
				break;
			default:
				throw new InternalCompilerException("Invalid Scanner state");
			}
		}

		tokenList.add(new Token(Terminal.SENTINEL));
		return tokenList;
	}

	private void accumulateLexical(char currentChar) {
		lexicalAccumulator.append(currentChar);
	}

	private void accumulateIntLiteral(char currentChar) {
		final int digit = Character.digit(currentChar, 10);
		numericalAccumulator = numericalAccumulator * 10 + digit;

		if (numericalAccumulator > Integer.MAX_VALUE) {
			throw new LexicalParserException("Integer literal too large");
		}
	}

	public static class Token implements Cloneable {
		private final Terminal terminal;

		public Token(Terminal terminal) {
			this.terminal = terminal;
		}

		public Terminal getTerminal() {
			return terminal;
		}

		@Override
		public String toString() {
			return terminal.toString();
		}

		@Override
		public Token clone() throws CloneNotSupportedException {
			return (Token) super.clone();
		}
	}

	public static class FlowModeToken extends Token {

		private FlowMode mode;

		public FlowModeToken(FlowMode mode) {
			super(Terminal.FLOWMODE);
			this.mode = mode;
		}

		@Override
		public String toString() {
			return "(" + super.toString() + ", " + mode + ")";
		}
	}

	public static class ChangeModeToken extends Token {
		private ChangeMode mode;

		public ChangeModeToken(ChangeMode mode) {
			super(Terminal.FLOWMODE);
			this.mode = mode;
		}

		@Override
		public String toString() {
			return "(" + super.toString() + ", " + mode + ")";
		}
	}

	public static class MechModeToken extends Token {
		private MechMode mode;

		public MechModeToken(MechMode mode) {
			super(Terminal.FLOWMODE);
			this.mode = mode;
		}

		@Override
		public String toString() {
			return "(" + super.toString() + ", " + mode + ")";
		}
	}

	public static class OperatorToken extends Token {
		private Operator operator;

		public OperatorToken(Terminal terminal, Operator operator) {
			super(terminal);
			this.operator = operator;
		}

		public Operator getOperator() {
			return operator;
		}

		@Override
		public String toString() {
			return "(" + super.toString() + ", " + operator + ")";
		}
	}

	public static class BoolLiteral extends Token {
		private final BoolValue value;

		public BoolLiteral(BoolValue value) {
			super(Terminal.LITERAL);
			this.value = value;
		}

		@Override
		public String toString() {
			return "(" + super.toString() + ", " + value + ")";
		}

	}

	public static class IntegerLiteral extends Token {
		private final int value;

		public IntegerLiteral(final int value) {
			super(Terminal.LITERAL);
			this.value = value;
		}

		public int getValue() {
			return value;
		}

		@Override
		public String toString() {
			return "(" + super.toString() + ", " + value + ")";
		}
	}

	public static class TypeToken extends Token {

		private Type type;

		public TypeToken(Type type) {
			super(Terminal.TYPE);
			this.type = type;
		}

		@Override
		public String toString() {
			return "(" + super.toString() + ", " + type + ")";
		}

	}

	public static class IdentifierToken extends Token {
		private final String identifier;

		public IdentifierToken(String identifier) {
			super(Terminal.IDENT);
			this.identifier = identifier;
		}

		public String getIdentifier() {
			return identifier;
		}

		@Override
		public String toString() {
			return "(" + super.toString() + ", \"" + identifier + "\")";
		}
	}

	public enum Terminal {
		LPAREN,
		RPAREN,
		COMMA,
		SEMICOLON,
		COLON,
		BECOMES,
		MULTOPR,
		ADDOPR,
		RELOPR,
		BOOLOPR,
		TYPE,
		WHILE,
		IDENT,
		LITERAL,
		CALL,
		DEBUGIN,
		DEBUGOUT,
		MECHMODE,
		CHANGEMODE,
		DO,
		ELSE,
		ENDFUN,
		ENDIF,
		ENDPROC,
		ENDPROGRAM,
		ENDWHILE,
		FUN,
		GLOBAL,
		IF,
		FLOWMODE,
		INIT,
		NOT,
		PROC,
		PROGRAM,
		RETURNS,
		SKIP,
		THEN,
		SENTINEL,
		LOCAL;

	}

	public enum Symbol {
		LPAREN("(", new Token(Terminal.LPAREN)),
		RPAREN(")", new Token(Terminal.RPAREN)),
		COMMA(",", new Token(Terminal.COMMA)),
		SEMICOLON(";", new Token(Terminal.SEMICOLON)),
		COLON(":", new Token(Terminal.COLON)),
		BECOMES(":=", new Token(Terminal.BECOMES)),
		TIMES("*", new OperatorToken(Terminal.MULTOPR, Operator.TIMES)), // MULTOPR
		PLUS("+", new OperatorToken(Terminal.ADDOPR, Operator.PLUS)), // ADDOPR
		MINUS("-", new OperatorToken(Terminal.ADDOPR, Operator.MINUS)), // ADDOPR
		EQ("=", new OperatorToken(Terminal.RELOPR, Operator.EQ)), // RELOPR
		NE("/=", new OperatorToken(Terminal.RELOPR, Operator.NE)), // RELOPR
		LT("<", new OperatorToken(Terminal.RELOPR, Operator.LT)), // RELOPR
		GT(">", new OperatorToken(Terminal.RELOPR, Operator.GT)), // RELOPR
		LE("<=", new OperatorToken(Terminal.RELOPR, Operator.LE)), // RELOPR
		GE(">=", new OperatorToken(Terminal.RELOPR, Operator.GE)), // RELOPR
		AND("&&", new OperatorToken(Terminal.BOOLOPR, Operator.AND)), // BOOLOPR
		OR("||", new OperatorToken(Terminal.BOOLOPR, Operator.OR)), // BOOLOPR
		CAND("&?", new OperatorToken(Terminal.BOOLOPR, Operator.CAND)), // BOOLOPR
		COR("|?", new OperatorToken(Terminal.BOOLOPR, Operator.COR)); // BOOLOPR

		private String symbol;
		private Token template;

		private Symbol(String symbol, Token template) {
			this.symbol = symbol;
			this.template = template;
		}

		public Token create() {
			try {
				return template.clone();
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}

			return null;
		}

		public static Symbol get(String s) {
			for (Symbol symbol : Symbol.values()) {
				if (symbol.symbol.equals(s)) {
					return symbol;
				}
			}

			return null;
		}

		public static boolean startsWith(char c) {
			for (Symbol symbol : Symbol.values()) {
				if (symbol.symbol.startsWith(String.valueOf(c))) {
					return true;
				}
			}

			return false;
		}

		public static boolean contains(String s) {
			for (Symbol symbol : Symbol.values()) {
				if (symbol.symbol.equals(s)) {
					return true;
				}
			}

			return false;
		}

	}

	public enum Keyword {
		BOOL("bool", new TypeToken(Type.BOOL)), // Type
		CALL("call", new Token(Terminal.CALL)),
		CONST("const", new ChangeModeToken(ChangeMode.CONST)), // ChangeMode
		COPY("copy", new MechModeToken(MechMode.COPY)), // MechMode
		DIV("div", new OperatorToken(Terminal.MULTOPR, Operator.DIV)),
		ELSE("else", new Token(Terminal.ELSE)),
		ENDWHILE("endwhile", new Token(Terminal.ENDWHILE)),
		FALSE("false", new BoolLiteral(BoolValue.FALSE)),
		FUN("fun", new Token(Terminal.FUN)),
		GLOBAL("global", new Token(Terminal.GLOBAL)),
		IF("if", new Token(Terminal.IF)),
		IN("in", new FlowModeToken(FlowMode.IN)), // FlowMode
		INIT("init", new Token(Terminal.INIT)),
		INOUT("inout", new FlowModeToken(FlowMode.INOUT)), // FlowMode
		INT32("int32", new TypeToken(Type.INT32)), // Type
		LOCAL("local", new Token(Terminal.LOCAL)),
		MOD("mod", new OperatorToken(Terminal.MULTOPR, Operator.MOD)),
		NOT("not", null), // TODO what is this?
		OUT("out", new FlowModeToken(FlowMode.OUT)), // FlowMode
		PROC("proc", new Token(Terminal.PROC)),
		PROGRAM("program", new Token(Terminal.PROGRAM)),
		REF("ref", new MechModeToken(MechMode.REF)), // MechMode
		RETURNS("returns", new Token(Terminal.RETURNS)),
		SKIP("skip", new Token(Terminal.SKIP)),
		TRUE("true", new BoolLiteral(BoolValue.TRUE)),
		VAR("var", new ChangeModeToken(ChangeMode.VAR)), // ChangeMode
		WHILE("while", new Token(Terminal.WHILE));

		private String keyword;
		private Token template;

		private Keyword(String keyword, Token template) {
			this.keyword = keyword;
			this.template = template;

		}

		public Token create() {
			try {
				return template.clone();
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}

			return null;
		}

		public static Keyword get(String s) {
			for (Keyword keyword : Keyword.values()) {
				if (keyword.keyword.equals(s)) {
					return keyword;
				}
			}

			return null;
		}

		public static boolean contains(String s) {
			for (Keyword keyword : Keyword.values()) {
				if (keyword.keyword.equals(s)) {
					return true;
				}
			}

			return false;
		}

	}

	public enum Operator {
		TIMES,
		DIV,
		MOD,
		PLUS,
		MINUS,
		EQ,
		NE,
		LT,
		GT,
		LE,
		GE,
		AND,
		OR,
		CAND,
		COR;
	}

	public enum ChangeMode {
		VAR,
		CONST;
	}

	public enum FlowMode {
		INOUT,
		IN,
		OUT;
	}

	public enum MechMode {
		COPY,
		REF;
	}

	public enum Type {
		BOOL,
		INT32;
	}

	public enum BoolValue {
		TRUE,
		FALSE
	}

	private static boolean isAlphaNumerical(char c) {
		return c >= 65 && c <= 122;
	}

	private static boolean isNumerical(char c) {
		return Character.isDigit(c);
	}

	public static void main(String[] args) {
		// List<Token> scan =
		// scan("Liebe while ref copy Grossmutter Zu Deinem 67 ten Geburtstag");

		final ImlScanner scanner = new ImlScanner();
		// List<Token> scan =
		// scanner.scan("Liebe while whiles<= < +++ while int32 ref copy Grossmutter Zu Deinem 67 ten Geburtstag");
		// List<Token> scan =
		// scanner.scan("while x36 <=67 do\nx := x-1\nendwhil\n");
		List<Token> scan = scanner.scan("while int <=<< 42 + while intis ha ha 3234 3*333*3+");
		// TODO does not accept last +
		System.out.println(scan.toString());

	}
}
