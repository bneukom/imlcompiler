package ch.fhnw.imlcompiler.scanner;

public class InternalCompilerException extends RuntimeException {

	public InternalCompilerException(String message) {
		super(message);
	}

}
